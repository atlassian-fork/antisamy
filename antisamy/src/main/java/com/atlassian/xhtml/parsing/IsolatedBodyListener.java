package com.atlassian.xhtml.parsing;

import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XNIException;

public interface IsolatedBodyListener
{
    public void completeForEndElement(QName element, Augmentations augs) throws XNIException;
}
